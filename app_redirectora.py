'''
Construir un programa en Python que sirva cualquier invocación que se le
realice con una redirección (códigos de resultado HTTP en el rango 3xx) a otro
recurso (aleatorio), que bien puede ser de sí mismo (como en el ejercicio 17.3) o
externo a partir de una lista con URLs.
'''
import argparse
import socket
import random
import time

PORT=1234

url_list = ['https://www.marca.com/', 'https://www.ikea.com/es/es/', 'https://es.wikipedia.org/wiki/Wikipedia:Portada',
            'https://github.com/', 'https://stackoverflow.com/', 'https://www.hp.com/es-es/home.html', 'https://www.zalando.es/',
            'https://www.zarahome.com/es/', 'https://www.casio.com/es/']

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=PORT)
    args = parser.parse_args()
    return args

def chosen_url():
    #me devuelve una url random de la lista o de mí mismo
    random_mine_url = "http://localhost:" + str(parse_args().port) + "/" + str(random.randint(1, 100000))
    random_url_list = random.choice(url_list)
    final_url = random.choice([random_mine_url, random_url_list])
    print(f"LA URL FINAL ES: {final_url}")
    return final_url

def main():
    args = parse_args()
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.bind(('localhost', args.port))
    print("serving at port", args.port)
    mySocket.listen(5)

    while True:
        print("Waiting for connections...")
        (recvSocket, address) = mySocket.accept()
        url = chosen_url()
        return_code = "302 Found"
        request = recvSocket.recv(2048).decode('utf-8')
        print(f"REQUEST: {request}")
        if 'localhost' in url:
            response = ('<html><head><meta http-equiv="refresh" content="5; URL=' + url + '"></head><body><h1>HA SIDO USTED REDIRECCIONADO A:' + url + '</h1></body></html>')

            recvSocket.send(bytes("HTTP/1.1 " + return_code + "\r\n\r\n" +
                                  response + '\r\n', 'utf-8'))


        else:

            recvSocket.send(bytes("HTTP/1.1 " + return_code + "\r\nLocation:" +
                              url + "\r\n\r\n", 'utf-8'))

        recvSocket.close()


if __name__ == "__main__":
    main()